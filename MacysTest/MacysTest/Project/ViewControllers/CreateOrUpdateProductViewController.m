//
//  CreateOrUpdateProductViewController.m
//  MacysTest
//
//  Created by Marcilio Junior on 11/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import "CreateOrUpdateProductViewController.h"
#import "EditProductCell.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface CreateOrUpdateProductViewController () <UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (nonatomic) NSArray *cellTitles;
@property (nonatomic) NSMutableArray *cellSubtitles;
@property (nonatomic) UITextField *activeTextField;

@end

@implementation CreateOrUpdateProductViewController

- (id)initWithNibFile
{
    return [super initWithNibName:@"CreateOrUpdateProductViewController" bundle:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.cellTitles = @[@"Name", @"Description", @"Regular Price", @"Sale Price", @"Avaliable Colors (separated by comma (,))", @"Stores"];
    self.cellSubtitles = [NSMutableArray arrayWithArray:@[self.product.name ? self.product.name : @"",
                           self.product.productDescription ? self.product.productDescription : @"",
                           self.product.regularPrice ? [NSString stringWithFormat:@"%.2f", [self.product.regularPrice doubleValue] / 100] : @"",
                           self.product.salePrice ? [NSString stringWithFormat:@"%.2f", [self.product.salePrice doubleValue] / 100] : @"",
                           self.product.colors ? [self.product.colors componentsJoinedByString:@","] : @"",
                           self.product.stores[@"name"] ? self.product.stores[@"name"] : @""]];
    
    self.tableView.rowHeight = 58;
    [self.tableView registerNib:[UINib nibWithNibName:@"EditProductCell"
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"EditProductCell_ID"];
    
    [self customizeHeaderView];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"OK" style:UIBarButtonItemStylePlain target:self action:@selector(saveOrUpdateProduct)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    if (!self.product) {
        self.product = [[Product alloc] init];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.product.productId ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.cellTitles.count;
    }
    else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"EditProductCell_ID";
        
        EditProductCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.labelTitle.text = self.cellTitles[indexPath.row];
        cell.labelTitle.textColor = [UIColor colorWithHex:@"#205FFA"];
        cell.textFieldProductField.text = self.cellSubtitles[indexPath.row];
        cell.textFieldProductField.delegate = self;
        cell.textFieldProductField.tag = indexPath.row;
        
        if (indexPath.row == 2 || indexPath.row == 3) {
            cell.textFieldProductField.keyboardType = UIKeyboardTypeDecimalPad;
        }
        
        return cell;
    }
    else {
        static NSString *CellIdentifier = @"DeleteProductCell_ID";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor redColor];
        cell.textLabel.text = @"Delete Product";
        
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Are you sure you want to delete this product?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alertView show];
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    self.product.photoPath = [[info objectForKey:UIImagePickerControllerReferenceURL] absoluteString];
    UIButton *buttonProductImage = (UIButton *)[self.tableView.tableHeaderView viewWithTag:101];
    [buttonProductImage setBackgroundImage:originalImage forState:UIControlStateNormal];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self updateTextFieldValue:textField];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self deleteProduct];
    }
}

#pragma mark - Helpers

- (void)customizeHeaderView {
    __block UIImage *photoProduct = nil;
    if ([self.product.photoPath hasPrefix:@"assets-library://"]) {
        NSURL *asseturl = [NSURL URLWithString:self.product.photoPath];
        ALAssetsLibrary *assetslibrary = [[ALAssetsLibrary alloc] init];
        [assetslibrary assetForURL:asseturl resultBlock:^(ALAsset *asset) {
            ALAssetRepresentation *representation = [asset defaultRepresentation];
            photoProduct = [UIImage imageWithCGImage:[representation fullResolutionImage]];
            
            [self headerWithImage:photoProduct];
        } failureBlock:nil];
    }
    else {
        photoProduct = [UIImage imageNamed:self.product.photoPath];
        
        [self headerWithImage:photoProduct];
    }
}

- (void)headerWithImage:(UIImage *)image {
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    
    UIButton *buttonProductImage = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonProductImage.frame = CGRectMake(320 / 2 - 30, 10, 60, 60);
    [buttonProductImage setBackgroundImage:image forState:UIControlStateNormal];
    [buttonProductImage addTarget:self action:@selector(chooseImage) forControlEvents:UIControlEventTouchUpInside];
    buttonProductImage.tag = 101;
    
    UIView *captionView = [[UIView alloc] initWithFrame:CGRectMake(0, 45, 60, 15)];
    captionView.backgroundColor = [UIColor colorWithHex:@"#A7A7A7" alpha:0.7];
    
    UILabel *labelCaption = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 15)];
    labelCaption.textColor = [UIColor whiteColor];
    labelCaption.font = [UIFont systemFontOfSize:10];
    labelCaption.textAlignment = NSTextAlignmentCenter;
    labelCaption.text = @"Choose";
    labelCaption.userInteractionEnabled = NO;
    
    [captionView addSubview:labelCaption];
    [buttonProductImage addSubview:captionView];
    
    [viewHeader addSubview:buttonProductImage];
    self.tableView.tableHeaderView = viewHeader;
}

- (void)saveOrUpdateProduct {
    [self updateTextFieldValue:self.activeTextField];
    
    for (NSInteger i = 0; i < self.cellTitles.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        EditProductCell *cell = (EditProductCell *)[self.tableView cellForRowAtIndexPath:indexPath];

        if (cell) {
            switch (i) {
                case 0:
                    self.product.name = cell.textFieldProductField.text;
                    break;
                case 1:
                    self.product.productDescription = cell.textFieldProductField.text;
                    break;
                case 2:
                    self.product.regularPrice = @([cell.textFieldProductField.text doubleValue] * 100);
                    break;
                case 3:
                    self.product.salePrice = @([cell.textFieldProductField.text doubleValue] * 100);
                    break;
                case 4:
                    self.product.colors = [cell.textFieldProductField.text componentsSeparatedByString:@","];
                    break;
                case 5:
                    self.product.stores = @{@"name": cell.textFieldProductField.text};
                    break;
                default:
                    break;
            }
        }
    }
    
    [[Product repository] insertOrUpdateProduct:self.product];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cancel {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)deleteProduct {
    [[Product repository] removeProductWithId:self.product.productId];
    
    UINavigationController* navigationController = (UINavigationController *)self.presentingViewController;
    [navigationController popViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)chooseImage {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)updateTextFieldValue:(UITextField *)textField {
    if (textField) {
        self.cellSubtitles[textField.tag] = textField.text;
        NSNumber *price = nil;
        
        switch (textField.tag) {
            case 0:
                self.product.name = textField.text;
                break;
            case 1:
                self.product.productDescription = textField.text;
                break;
            case 2:
                self.product.regularPrice = @([textField.text doubleValue] * 100);
                break;
            case 3:
                if ([textField.text rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound) {
                    price = @([textField.text doubleValue] * 100);
                }
                else {
                    price = @([textField.text doubleValue]);
                }
                
                self.product.salePrice = @([textField.text doubleValue] * 100);
                break;
            case 4:
                self.product.colors = [textField.text componentsSeparatedByString:@","];
                break;
            case 5:
                self.product.stores = @{@"name": textField.text};
                break;
            default:
                break;
        }
    }
}

@end
