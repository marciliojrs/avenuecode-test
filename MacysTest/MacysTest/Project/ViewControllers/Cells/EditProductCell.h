//
//  EditProductCell.h
//  MacysTest
//
//  Created by Marcilio Junior on 11/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProductCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UITextField *textFieldProductField;

@end
