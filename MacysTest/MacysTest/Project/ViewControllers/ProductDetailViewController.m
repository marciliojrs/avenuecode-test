//
//  ProductDetailViewController.m
//  MacysTest
//
//  Created by Marcilio Junior on 11/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "CreateOrUpdateProductViewController.h"
#import "MWPhotoBrowser.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface ProductDetailViewController () <MWPhotoBrowserDelegate>

@property (nonatomic) NSArray *cellTitles;
@property (nonatomic) NSArray *cellSubtitles;
@property (nonatomic) Product *product;
@property (nonatomic) UIImage *productPhoto;

@end

@implementation ProductDetailViewController

- (id)initWithNibFile
{
    return [super initWithNibName:@"ProductDetailViewController" bundle:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.product = [[Product repository] productWithId:self.productId];
    [self customizeView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cellTitles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ProductDetailCell_ID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.font = [UIFont systemFontOfSize:10];
        cell.textLabel.textColor = [UIColor colorWithHex:@"#205FFA"];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
        cell.detailTextLabel.numberOfLines = 0;
    }
    
    cell.textLabel.text = self.cellTitles[indexPath.row];
    cell.detailTextLabel.text = self.cellSubtitles[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellText = self.cellSubtitles[indexPath.row];
    CGSize size;
    
    if (SYSTEM_VERSION_LESS_THAN(@"7")) {
        size = [cellText sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(300, MAXFLOAT)];
    }
    else {
        CGRect rect = [cellText boundingRectWithSize:CGSizeMake(300, MAXFLOAT)
                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                          attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]}
                                                             context:nil];
        size = rect.size;
    }
    
    return MAX(size.height + 28, 44);
}

#pragma mark - Helpers

- (void)customizeView {
    if (self.product) {
        self.cellTitles = @[@"Name", @"Description", @"Regular Price", @"Sale Price", @"Avaliable Colors", @"Stores"];
        self.cellSubtitles = @[self.product.name,
                               self.product.productDescription,
                               [NSString stringWithFormat:@"%.2f", [self.product.regularPrice doubleValue] / 100],
                               [NSString stringWithFormat:@"%.2f", [self.product.salePrice doubleValue] / 100],
                               [self.product.colors componentsJoinedByString:@","],
                               self.product.stores[@"name"] ? self.product.stores[@"name"] : @""];
        
        self.tableView.tableFooterView = [[UIView alloc] init];
        self.title = self.product.name;
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(editProduct)];
        
        __block UIImage *photoProduct = nil;
        if ([self.product.photoPath hasPrefix:@"assets-library://"]) {
            NSURL *asseturl = [NSURL URLWithString:self.product.photoPath];
            ALAssetsLibrary *assetslibrary = [[ALAssetsLibrary alloc] init];
            [assetslibrary assetForURL:asseturl resultBlock:^(ALAsset *asset) {
                ALAssetRepresentation *representation = [asset defaultRepresentation];
                photoProduct = [UIImage imageWithCGImage:[representation fullResolutionImage]];
                
                [self headerWithImage:photoProduct];
            } failureBlock:nil];
        }
        else {
            photoProduct = [UIImage imageNamed:self.product.photoPath];
            
            [self headerWithImage:photoProduct];
        }                
        
        [self.tableView reloadData];
    }
}

- (void)editProduct {
    CreateOrUpdateProductViewController *editProductViewController = [[CreateOrUpdateProductViewController alloc] initWithNibFile];
    editProductViewController.product = self.product;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:editProductViewController];    
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)openImage {
    if (self.product.photoPath.length > 0) {
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        
        browser.displayActionButton = NO;
        browser.displayNavArrows = NO;
        browser.displaySelectionButtons = NO;
        browser.zoomPhotosToFill = YES;
        browser.alwaysShowControls = NO;
        browser.enableGrid = NO;
        browser.startOnGrid = NO;
        browser.wantsFullScreenLayout = YES;
        
        [self.navigationController pushViewController:browser animated:YES];
    }
}

- (void)headerWithImage:(UIImage *)image {
    if (image) {
        self.productPhoto = image;
        UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
        
        UIButton *buttonProductImage = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonProductImage.frame = CGRectMake(320 / 2 - 30, 10, 60, 60);
        [buttonProductImage setBackgroundImage:image forState:UIControlStateNormal];
        [buttonProductImage addTarget:self action:@selector(openImage) forControlEvents:UIControlEventTouchUpInside];
        
        [viewHeader addSubview:buttonProductImage];
        self.tableView.tableHeaderView = viewHeader;
    }
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.product.photoPath.length > 0 ? 1 : 0;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    return [MWPhoto photoWithImage:self.productPhoto];
}

@end
