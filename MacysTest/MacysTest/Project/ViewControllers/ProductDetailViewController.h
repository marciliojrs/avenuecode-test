//
//  ProductDetailViewController.h
//  MacysTest
//
//  Created by Marcilio Junior on 11/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface ProductDetailViewController : UITableViewController

@property (nonatomic) NSNumber *productId;

- (id)initWithNibFile;

@end
