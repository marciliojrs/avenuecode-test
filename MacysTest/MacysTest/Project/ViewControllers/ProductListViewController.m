//
//  ProductListViewController.m
//  MacysTest
//
//  Created by Marcilio Junior on 11/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductDetailViewController.h"
#import "CreateOrUpdateProductViewController.h"
#import "Product.h"

@interface ProductListViewController ()

@property (nonatomic) NSArray *products;

@end

@implementation ProductListViewController

- (id)initWithNibFile
{
    return [super initWithNibName:@"ProductListViewController" bundle:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Products";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewProduct)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self fetchProductsList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ProductListCell_ID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    Product* product = self.products[indexPath.row];
    cell.textLabel.text = product.name;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ProductDetailViewController *productDetailController = [[ProductDetailViewController alloc] initWithNibFile];
    Product *product = self.products[indexPath.row];
    productDetailController.productId = product.productId;
    
    [self.navigationController pushViewController:productDetailController animated:YES];
}

#pragma mark - Helpers

- (void)fetchProductsList {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        self.products = [[Product repository] listAllProducts];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    });
}

- (void)addNewProduct {
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[CreateOrUpdateProductViewController alloc] initWithNibFile]];
    
    [self presentViewController:navigationController animated:YES completion:nil];
}

@end
