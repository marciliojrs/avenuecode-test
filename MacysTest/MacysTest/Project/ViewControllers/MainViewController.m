//
//  MainViewController.m
//  MacysTest
//
//  Created by Marcilio Junior on 11/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import "MainViewController.h"
#import "ProductListViewController.h"
#import "ProductService.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)fetchData:(id)sender {
    [[ProductService service] fetchProductsAndSave];
}

- (IBAction)seeProductsList:(id)sender {
    ProductListViewController *productListViewController = [[ProductListViewController alloc] initWithNibFile];
    [self.navigationController pushViewController:productListViewController animated:YES];
}
@end
