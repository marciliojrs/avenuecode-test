//
//  ProductListViewController.h
//  MacysTest
//
//  Created by Marcilio Junior on 11/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductListViewController : UITableViewController

- (id)initWithNibFile;

@end
