//
//  UIColor+HexColor.h
//  SQFramework
//
//  Created by Marcilio Junior on 09/07/13.
//  Copyright (c) 2013 Squadra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColor)

+ (UIColor*)colorWith8BitRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha;
+ (UIColor*)colorWithHex:(NSString*)hex alpha:(CGFloat)alpha;
+ (UIColor*)colorWithHex:(NSString*)hex;

@end
