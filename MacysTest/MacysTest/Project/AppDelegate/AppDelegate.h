//
//  AppDelegate.h
//  MacysTest
//
//  Created by Marcilio Junior on 10/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
