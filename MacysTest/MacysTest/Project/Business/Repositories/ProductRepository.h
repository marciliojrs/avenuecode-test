//
//  ProductRepository.h
//  MacysTest
//
//  Created by Marcilio Junior on 10/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Product;
@class FMDatabase;

@interface ProductRepository : NSObject

- (BOOL)insertOrUpdateProduct:(Product *)product;
- (BOOL)removeProductWithId:(NSNumber *)productId;
- (Product *)productWithId:(NSNumber *)productId;
- (NSArray *)listAllProducts;

@end

@interface ProductRepository (Database)

+ (FMDatabase *)database;
+ (void)createDatabaseStructure:(FMDatabase *)database;

@end