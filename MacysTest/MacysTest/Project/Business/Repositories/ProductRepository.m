//
//  ProductRepository.m
//  MacysTest
//
//  Created by Marcilio Junior on 10/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import "ProductRepository.h"
#import "FMDatabase.h"
#import "Product.h"

static NSString * const ACMDatabaseName = @"macystest.sqlite";

@implementation ProductRepository

- (BOOL)insertOrUpdateProduct:(Product *)product {
    
    NSString *sqlFormat = @"INSERT OR REPLACE INTO Product (%@)"
                                            " VALUES (%@)";
    
    NSMutableArray *fields = [NSMutableArray arrayWithArray:@[@"name", @"description", @"regularPrice", @"salePrice", @"stores", @"colors", @"photoPath"]];
    NSMutableArray *interrogationMarks = [NSMutableArray arrayWithArray:@[@"?", @"?", @"?", @"?", @"?", @"?", @"?"]];
    
    NSData* storesData = [NSKeyedArchiver archivedDataWithRootObject:product.stores];
    NSData* colorsData = [NSKeyedArchiver archivedDataWithRootObject:product.colors];
    
    NSMutableArray *arguments = [NSMutableArray arrayWithArray:@[product.name ? product.name : @"",
                                                                 product.productDescription ? product.productDescription : @"",
                                                                 product.regularPrice ? product.regularPrice : @"",
                                                                 product.salePrice ? product.salePrice : @"",
                                                                 storesData,
                                                                 colorsData,
                                                                 product.photoPath ? product.photoPath : @""]];
    
    if (product.productId) {
        [fields insertObject:@"id" atIndex:0];
        [arguments insertObject:product.productId atIndex:0];
        [interrogationMarks addObject:@"?"];
    }
    
    NSString *sql = [NSString stringWithFormat:sqlFormat, [fields componentsJoinedByString:@","], [interrogationMarks componentsJoinedByString:@","]];
    
    BOOL succeeded = [[ProductRepository database] executeUpdate:sql
                                            withArgumentsInArray:arguments];
    
    if (!succeeded) {
        NSLog(@"[DATABASE ERROR]: %@", [[ProductRepository database] lastErrorMessage]);
    }
    
    return succeeded;
}

- (BOOL)removeProductWithId:(NSNumber *)productId {
    NSString *sql = @"DELETE FROM Product WHERE id = ?";
    NSArray *arguments = @[productId];
    
    BOOL succeeded = [[ProductRepository database] executeUpdate:sql
                                            withArgumentsInArray:arguments];
    
    if (!succeeded) {
        NSLog(@"[DATABASE ERROR]: %@", [[ProductRepository database] lastErrorMessage]);
    }
    
    return succeeded;
}

- (Product *)productWithId:(NSNumber *)productId {
    Product* product = nil;
    
    NSString *sql = @"SELECT * FROM Product WHERE id = ?";
    NSArray *arguments = @[productId];
    
    FMResultSet *resultSet = [[ProductRepository database] executeQuery:sql                              
                                                   withArgumentsInArray:arguments];
    
    if ([resultSet next]) {
        product = [Product objectWithDictionary:[resultSet resultDictionary]];
    }
    
    [resultSet close];
    
    return product;
}

- (NSArray *)listAllProducts {
    NSMutableArray *products = [NSMutableArray array];
    
    NSString *sql = @"SELECT * FROM Product";
    
    FMResultSet *resultSet = [[ProductRepository database] executeQuery:sql];
    
    while ([resultSet next]) {
        Product *product = [Product objectWithDictionary:[resultSet resultDictionary]];
        
        if (product) {
            [products addObject:product];
        }
    }
    
    [resultSet close];
    
    return products;
}

@end

@implementation ProductRepository (Database)

+ (FMDatabase *)database {
    static FMDatabase *sharedDatabase = nil;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *documentsDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:ACMDatabaseName];
        
        BOOL newDatabase = ![[NSFileManager defaultManager] fileExistsAtPath:databasePath];
//        if ([[NSFileManager defaultManager] fileExistsAtPath:databasePath]) {
//            [[NSFileManager defaultManager] removeItemAtPath:databasePath error:nil];
//        }
        
        sharedDatabase = [FMDatabase databaseWithPath:databasePath];
        
        if (![sharedDatabase open]) {
            sharedDatabase = nil;
            NSLog(@"[DATABASE ERROR]: %@", [sharedDatabase lastErrorMessage]);
        }
    
        if (newDatabase) {
            [ProductRepository createDatabaseStructure:sharedDatabase];
        }
    });
                  
    return sharedDatabase;
}

+ (void)createDatabaseStructure:(FMDatabase *)database
{
    NSString* sql = @"CREATE TABLE Product (id INTEGER PRIMARY KEY, name VARCHAR(50), description VARCHAR(200), photoPath VARCHAR(500), regularPrice INTEGER, salePrice INTEGER, stores TEXT, colors TEXT);";
    
    BOOL succeeded = [database executeUpdate:sql];
    
    if (!succeeded) {
        NSLog(@"[DATABASE ERROR]: %@", [[ProductRepository database] lastErrorMessage]);
    }
}

@end
