//
//  ProductService.h
//  MacysTest
//
//  Created by Marcilio Junior on 10/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ListProductsBlockDef)(NSArray* products, NSError* error);

@interface ProductService : NSObject

+ (instancetype)service;

- (void)fetchProductsAndSave;

@end
