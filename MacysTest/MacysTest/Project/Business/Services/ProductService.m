//
//  ProductService.m
//  MacysTest
//
//  Created by Marcilio Junior on 10/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import "ProductService.h"
#import "WSMockClient.h"
#import "Product.h"

@implementation ProductService

+ (instancetype)service {
    static id sharedService = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[self alloc] init];
    });
    
    return sharedService;
}

- (void)fetchProductsAndSave {
    WSMockClient* client = [WSMockClient sharedClient];
    
    [client listAllProducts:^(id mockResponse, NSError *error) {
        NSMutableArray* products = nil;
        
        if (!error) {
            NSArray* productsResponse = [mockResponse objectForKey:@"products"];
            
            products = [NSMutableArray arrayWithCapacity:productsResponse.count];
            for (NSDictionary* productJson in productsResponse) {
                Product* product = [[Product alloc] initWithDictionary:productJson
                                                                 error:nil];
                
                if (product) {
                    [[Product repository] insertOrUpdateProduct:product];
                }
            }
        }
        else {
            NSLog(@"[MOCK ERROR]: %@", [error localizedDescription]);
        }
    }];
}

@end
