//
//  WSMockClient.m
//  MacysTest
//
//  Created by Marcilio Junior on 10/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import "WSMockClient.h"

@implementation WSMockClient

+ (instancetype)sharedClient {
    static id sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[self alloc] init];
    });
    
    return sharedClient;
}

- (void)listAllProducts:(WSMockClientResponse)completion
{
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"serviceResponse" ofType:@"json"];
    
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfFile:jsonPath options:NSDataReadingMappedIfSafe error:&error];
    
    if (error) {
        completion(nil, error);
    }
    else {
        id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
        
        completion(jsonObject, error);
    }
}

@end
