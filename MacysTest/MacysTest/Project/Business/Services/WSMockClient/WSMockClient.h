//
//  WSMockClient.h
//  MacysTest
//
//  Created by Marcilio Junior on 10/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^WSMockClientResponse)(id mockResponse, NSError* error);

@interface WSMockClient : NSObject

+ (instancetype)sharedClient;

- (void)listAllProducts:(WSMockClientResponse)completion;

@end
