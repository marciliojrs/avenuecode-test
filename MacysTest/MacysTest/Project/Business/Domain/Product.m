//
//  Product.m
//  MacysTest
//
//  Created by Marcilio Junior on 10/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import "Product.h"

@implementation Product

+ (JSONKeyMapper *)keyMapper {
    NSDictionary* customMapper = @{@"id": @"productId",
                                   @"description": @"productDescription"};
    
    return [[JSONKeyMapper alloc] initWithDictionary:customMapper];
}

+ (instancetype)objectWithDictionary:(NSDictionary *)dictionary {
    Product *product = [[self alloc] init];
    
    product.productId = [dictionary objectForKey:@"id"];
    product.productDescription = [dictionary objectForKey:@"description"];
    product.salePrice = [dictionary objectForKey:@"salePrice"];
    product.name = [dictionary objectForKey:@"name"];
    product.regularPrice = [dictionary objectForKey:@"regularPrice"];
    product.photoPath = [dictionary objectForKey:@"photoPath"];
    
    id stores = [dictionary objectForKey:@"stores"];
    if ([stores isKindOfClass:NSData.class]) {
        product.stores = [NSKeyedUnarchiver unarchiveObjectWithData:stores];
    }
    else {
        product.stores = stores;
    }
    
    id colors = [dictionary objectForKey:@"colors"];
    if ([colors isKindOfClass:NSData.class]) {
        product.colors = [NSKeyedUnarchiver unarchiveObjectWithData:colors];
    }
    else {
        product.colors = colors;
    }
    
    return product;
}

+ (ProductRepository *)repository {
    static id sharedRepository = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedRepository = [[ProductRepository alloc] init];
    });
    
    return sharedRepository;
}

@end
