//
//  Product.h
//  MacysTest
//
//  Created by Marcilio Junior on 10/03/14.
//  Copyright (c) 2014 AvenueCode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductRepository.h"

@interface Product : JSONModel

@property (nonatomic) NSNumber<Optional> *productId;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *productDescription;
@property (nonatomic) NSNumber *regularPrice;
@property (nonatomic) NSNumber *salePrice;
@property (nonatomic) NSString *photoPath;
@property (nonatomic) NSArray *colors;
@property (nonatomic) NSDictionary *stores;

+ (instancetype)objectWithDictionary:(NSDictionary *)dictionary;

+ (ProductRepository *)repository;

@end
